package app.ecommerce.ui.activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import app.ecommerce.ui.R;
import app.ecommerce.ui.adapter.DayPlanAdapter;
import app.ecommerce.ui.data.DataGenerator;
import app.ecommerce.ui.fixtures.DayPlanFixtures;
import app.ecommerce.ui.model.DayPlanItem;

public class ActivityDayPlans extends AppCompatActivity {
    private RecyclerView rvDayPlans;
    private DayPlanAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.day_plans_activity);
        initToolbar();
        initRecyclerView();
        initData();
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    private void initRecyclerView() {
        rvDayPlans = findViewById(R.id.rvDayPlans);
        adapter = new DayPlanAdapter(this);

        rvDayPlans.setLayoutManager(new LinearLayoutManager(this));
        rvDayPlans.setAdapter(adapter);
    }

    private void initData() {
        List<DayPlanItem> items = new ArrayList<>();

        items.add(new DayPlanItem(DayPlanFixtures.LABEL_TYPE, "Suggested"));
        items.add(DataGenerator.getThreeDaysInParisSights());
        items.add(DataGenerator.getMainSights());
        items.add(DataGenerator.getCityHighlightSights());

        adapter.setItems(items);
    }
}

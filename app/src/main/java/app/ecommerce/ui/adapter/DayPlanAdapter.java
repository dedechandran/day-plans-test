package app.ecommerce.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import app.ecommerce.ui.R;
import app.ecommerce.ui.fixtures.DayPlanFixtures;
import app.ecommerce.ui.model.DayPlan;
import app.ecommerce.ui.model.DayPlanItem;
import app.ecommerce.ui.model.DayPlanSight;

public class DayPlanAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<DayPlanItem> items = new ArrayList<>();

    public DayPlanAdapter(Context context) {
        this.context = context;
    }

    public void setItems(List<DayPlanItem> items) {
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return viewType == DayPlanFixtures.LABEL_TYPE ?
                new DayPlanLabelViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.day_plans_label_item, parent, false)) :
                new DayPlanViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.day_plans_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof DayPlanLabelViewHolder) {
            DayPlanLabelViewHolder view = (DayPlanLabelViewHolder) holder;
            String item = (String) items.get(position).getDayPlanItem();

            view.dayPlanLabel.setText(item);
        } else if (holder instanceof DayPlanViewHolder) {
            final DayPlanViewHolder view = (DayPlanViewHolder) holder;
            final DayPlan item = (DayPlan) items.get(position).getDayPlanItem();
            DayPlanImageAdapter adapter = new DayPlanImageAdapter();

            view.dayPlanTitle.setText(item.getDayPlanTitle());

            StringBuilder builder = new StringBuilder();
            builder.append(item.getDayPlanNumberOfSight());
            builder.append(' ');
            builder.append(context.getString(R.string.day_plan_number_of_sight_description));
            view.dayPlanSight.setText(builder);

            view.dayPlanImages.setLayoutManager(new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false));
            view.dayPlanImages.setAdapter(adapter);
            adapter.setItems(item.getDayPlanImages());

            view.dayPlanDropDown.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (view.dayPlanContainer.getChildCount() == DayPlanFixtures.DAY_PLAN_CONTAINER_CHILD_COUNT) {
                        view.dayPlanDropDown.setImageResource(R.drawable.ic_keyboard_arrow_up_grey_24dp);
                        for (int i = 0; i < item.getDayPlanSights().size(); i++) {
                            DayPlanSight sight = item.getDayPlanSights().get(i);

                            View sightDetail = LayoutInflater.from(view.itemView.getContext()).inflate(R.layout.day_plan_sight_detail_item, null);
                            TextView sightName = sightDetail.findViewById(R.id.textDayPlanSightName);
                            TextView sightDistance = sightDetail.findViewById(R.id.textDayPlanSightDistance);
                            TextView sightTime = sightDetail.findViewById(R.id.textDayPlanSightTime);
                            TextView sightDuration = sightDetail.findViewById(R.id.textDayPlanSightDuration);
                            TextView sightNumber = sightDetail.findViewById(R.id.textDayPlanSightNumber);

                            if (i % 2 == 0) {
                                sightDetail.setBackgroundColor(ContextCompat.getColor(context, R.color.grey_3));
                            }

                            if (sight.getDayPlanSightDistance() != 0) {
                                sightDistance.setVisibility(View.VISIBLE);
                                StringBuilder builder = new StringBuilder();
                                builder.append(sight.getDayPlanSightDistance());
                                builder.append(' ');
                                builder.append(context.getString(R.string.day_plan_distance_description));
                                sightDistance.setText(builder);
                            }

                            sightNumber.setText(String.valueOf(i + 1));

                            if (!sight.getDayPlanSightTime().isEmpty()) {
                                sightTime.setVisibility(View.VISIBLE);
                                StringBuilder builder = new StringBuilder();
                                builder.append(context.getString(R.string.day_plan_time_description));
                                builder.append(' ');
                                builder.append(sight.getDayPlanSightTime());
                                sightTime.setText(builder);
                            }

                            sightName.setText(sight.getDayPlanSightName());

                            StringBuilder builder = new StringBuilder();
                            builder.append(context.getString(R.string.day_plan_duration_description_1));
                            builder.append(' ');
                            builder.append(sight.getDayPlanSightDuration());
                            builder.append(' ');
                            builder.append(context.getString(R.string.day_plan_duration_description_2));
                            sightDuration.setText(builder);

                            view.dayPlanContainer.addView(sightDetail);
                        }

                    } else {
                        view.dayPlanDropDown.setImageResource(R.drawable.ic_keyboard_arrow_down_grey_24dp);
                        view.dayPlanContainer.removeViews(DayPlanFixtures.DAY_PLAN_CONTAINER_CHILD_COUNT,
                                view.dayPlanContainer.getChildCount() - DayPlanFixtures.DAY_PLAN_CONTAINER_CHILD_COUNT);
                    }

                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getDayPlanType() == DayPlanFixtures.LABEL_TYPE ? DayPlanFixtures.LABEL_TYPE : DayPlanFixtures.DAY_PLAN_ITEM_TYPE;
    }

    class DayPlanLabelViewHolder extends RecyclerView.ViewHolder {
        TextView dayPlanLabel;

        DayPlanLabelViewHolder(@NonNull View itemView) {
            super(itemView);
            dayPlanLabel = itemView.findViewById(R.id.textDayPlanLabel);
        }
    }

    class DayPlanViewHolder extends RecyclerView.ViewHolder {
        TextView dayPlanTitle;
        TextView dayPlanSight;
        RecyclerView dayPlanImages;
        LinearLayout dayPlanContainer;
        ImageView dayPlanDropDown;

        DayPlanViewHolder(@NonNull View itemView) {
            super(itemView);
            dayPlanTitle = itemView.findViewById(R.id.textDayPlanTitle);
            dayPlanSight = itemView.findViewById(R.id.textDayPlanSight);
            dayPlanImages = itemView.findViewById(R.id.rvDayPlanImages);
            dayPlanContainer = itemView.findViewById(R.id.dayPlanContainer);
            dayPlanDropDown = itemView.findViewById(R.id.imageDayPlanInfoDropDown);
        }

    }
}

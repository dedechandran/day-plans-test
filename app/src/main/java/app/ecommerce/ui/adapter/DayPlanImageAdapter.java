package app.ecommerce.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import app.ecommerce.ui.R;
import app.ecommerce.ui.fixtures.DayPlanFixtures;
import app.ecommerce.ui.model.DayPlanImage;

public class DayPlanImageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<DayPlanImage> items = new ArrayList<>();

    public void setItems(List<DayPlanImage> items) {
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return viewType == DayPlanFixtures.MAPS_IMAGE_TYPE ?
                new MapImageViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.day_plan_map_image_item, parent, false)) :
                new SightImageViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.day_plan_sight_image_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MapImageViewHolder) {
            MapImageViewHolder view = (MapImageViewHolder) holder;
            view.imageDayPlanMap.setImageResource(items.get(position).getDayPlanImageResource());
        } else if (holder instanceof SightImageViewHolder) {
            SightImageViewHolder view = (SightImageViewHolder) holder;
            view.imageDayPlanSight.setImageResource(items.get(position).getDayPlanImageResource());
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getDayPlanImageType() == DayPlanFixtures.MAPS_IMAGE_TYPE ? DayPlanFixtures.MAPS_IMAGE_TYPE : DayPlanFixtures.SIGHT_IMAGE_TYPE;
    }

    class MapImageViewHolder extends RecyclerView.ViewHolder {
        ImageView imageDayPlanMap;

        MapImageViewHolder(@NonNull View itemView) {
            super(itemView);
            imageDayPlanMap = itemView.findViewById(R.id.imageDayPlanMap);
        }
    }

    class SightImageViewHolder extends RecyclerView.ViewHolder {
        ImageView imageDayPlanSight;

        SightImageViewHolder(@NonNull View itemView) {
            super(itemView);
            imageDayPlanSight = itemView.findViewById(R.id.imageDayPlanSight);
        }
    }
}

package app.ecommerce.ui.data;

import android.content.Context;
import android.content.res.TypedArray;

import java.util.ArrayList;
import java.util.List;

import app.ecommerce.ui.R;
import app.ecommerce.ui.fixtures.DayPlanFixtures;
import app.ecommerce.ui.model.DayPlan;
import app.ecommerce.ui.model.DayPlanImage;
import app.ecommerce.ui.model.DayPlanItem;
import app.ecommerce.ui.model.DayPlanSight;
import app.ecommerce.ui.model.ExampleProduct;

@SuppressWarnings("ResourceType")
public class DataGenerator {

    /**
     * Generate dummy data shopping product
     *
     * @param ctx android context
     * @return list of object
     */
    public static List<ExampleProduct> getProducts(Context ctx) {
        List<ExampleProduct> items = new ArrayList<>();
        TypedArray drw_arr = ctx.getResources().obtainTypedArray(R.array.product_image);
        String title_arr[] = ctx.getResources().getStringArray(R.array.product_title);
        String price_arr[] = ctx.getResources().getStringArray(R.array.product_price);
        for (int i = 0; i < drw_arr.length(); i++) {
            ExampleProduct obj = new ExampleProduct();
            obj.image = drw_arr.getResourceId(i, -1);
            obj.title = title_arr[i];
            obj.price = price_arr[i];
            obj.imageDrw = ctx.getResources().getDrawable(obj.image);
            items.add(obj);
        }
        return items;
    }

    public static DayPlanItem getThreeDaysInParisSights(){
        List<DayPlanImage> images = new ArrayList<>();
        List<DayPlanSight> sights = new ArrayList<>();

        sights.add(new DayPlanSight(0,"Eiffel Tower",45,""));
        sights.add(new DayPlanSight(9,"The Arc de Triomphe",15,"09:00"));
        sights.add(new DayPlanSight(12,"Louvre",5,""));
        sights.add(new DayPlanSight(0,"Cathedral Notherdam",10,"10:00"));
        sights.add(new DayPlanSight(15,"Moulin Rouge",10,"18:00"));

        images.add(new DayPlanImage(DayPlanFixtures.MAPS_IMAGE_TYPE,R.drawable.image_maps));
        images.add(new DayPlanImage(DayPlanFixtures.SIGHT_IMAGE_TYPE,R.drawable.image_day_plan_1));
        images.add(new DayPlanImage(DayPlanFixtures.SIGHT_IMAGE_TYPE,R.drawable.image_day_plan_2));
        images.add(new DayPlanImage(DayPlanFixtures.SIGHT_IMAGE_TYPE,R.drawable.image_day_plan_3));
        images.add(new DayPlanImage(DayPlanFixtures.SIGHT_IMAGE_TYPE,R.drawable.image_day_plan_4));
        images.add(new DayPlanImage(DayPlanFixtures.SIGHT_IMAGE_TYPE,R.drawable.image_day_plan_5));

        return new DayPlanItem(DayPlanFixtures.DAY_PLAN_ITEM_TYPE, new DayPlan("Three Days in Paris",sights.size(),images,sights));
    }

    public static DayPlanItem getMainSights(){
        List<DayPlanImage> images = new ArrayList<>();
        List<DayPlanSight> sights = new ArrayList<>();

        sights.add(new DayPlanSight(0,"Eiffel Tower",45,""));
        sights.add(new DayPlanSight(9,"The Arc de Triomphe",15,"09:00"));
        sights.add(new DayPlanSight(12,"Louvre",5,""));

        images.add(new DayPlanImage(DayPlanFixtures.MAPS_IMAGE_TYPE,R.drawable.image_maps));
        images.add(new DayPlanImage(DayPlanFixtures.SIGHT_IMAGE_TYPE,R.drawable.image_day_plan_1));
        images.add(new DayPlanImage(DayPlanFixtures.SIGHT_IMAGE_TYPE,R.drawable.image_day_plan_2));
        images.add(new DayPlanImage(DayPlanFixtures.SIGHT_IMAGE_TYPE,R.drawable.image_day_plan_3));

        return new DayPlanItem(DayPlanFixtures.DAY_PLAN_ITEM_TYPE, new DayPlan("Main Sights",sights.size(),images,sights));
    }

    public static DayPlanItem getCityHighlightSights(){
        List<DayPlanImage> images = new ArrayList<>();
        List<DayPlanSight> sights = new ArrayList<>();

        sights.add(new DayPlanSight(0,"Eiffel Tower",45,""));
        sights.add(new DayPlanSight(9,"The Arc de Triomphe",15,"09:00"));
        sights.add(new DayPlanSight(12,"Louvre",5,""));
        sights.add(new DayPlanSight(0,"Cathedral Notherdam",10,"10:00"));

        images.add(new DayPlanImage(DayPlanFixtures.MAPS_IMAGE_TYPE,R.drawable.image_maps));
        images.add(new DayPlanImage(DayPlanFixtures.SIGHT_IMAGE_TYPE,R.drawable.image_day_plan_1));
        images.add(new DayPlanImage(DayPlanFixtures.SIGHT_IMAGE_TYPE,R.drawable.image_day_plan_2));
        images.add(new DayPlanImage(DayPlanFixtures.SIGHT_IMAGE_TYPE,R.drawable.image_day_plan_3));
        images.add(new DayPlanImage(DayPlanFixtures.SIGHT_IMAGE_TYPE,R.drawable.image_day_plan_4));

        return new DayPlanItem(DayPlanFixtures.DAY_PLAN_ITEM_TYPE, new DayPlan("City Center Highlights",sights.size(),images,sights));
    }
}

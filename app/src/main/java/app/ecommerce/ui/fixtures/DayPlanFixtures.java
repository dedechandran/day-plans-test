package app.ecommerce.ui.fixtures;

public class DayPlanFixtures {
    public static final int LABEL_TYPE = 1;
    public static final int DAY_PLAN_ITEM_TYPE = 2;
    public static final int MAPS_IMAGE_TYPE = 3;
    public static final int SIGHT_IMAGE_TYPE = 4;
    public static final int DAY_PLAN_CONTAINER_CHILD_COUNT = 3;
}

package app.ecommerce.ui.model;

import java.util.List;

public class DayPlan {
    private String dayPlanTitle;
    private int dayPlanNumberOfSight;
    private List<DayPlanImage> dayPlanImages;
    private List<DayPlanSight> dayPlanSights;

    public DayPlan(String dayPlanTitle, int dayPlanNumberOfSight,List<DayPlanImage> dayPlanImages,List<DayPlanSight> dayPlanSights){
        this.dayPlanTitle = dayPlanTitle;
        this.dayPlanNumberOfSight = dayPlanNumberOfSight;
        this.dayPlanImages = dayPlanImages;
        this.dayPlanSights = dayPlanSights;
    }

    public String getDayPlanTitle() {
        return dayPlanTitle;
    }

    public void setDayPlanTitle(String dayPlanTitle) {
        this.dayPlanTitle = dayPlanTitle;
    }

    public int getDayPlanNumberOfSight() {
        return dayPlanNumberOfSight;
    }

    public void setDayPlanNumberOfSight(int dayPlanNumberOfSight) {
        this.dayPlanNumberOfSight = dayPlanNumberOfSight;
    }

    public List<DayPlanImage> getDayPlanImages() {
        return dayPlanImages;
    }

    public void setDayPlanImages(List<DayPlanImage> dayPlanImages) {
        this.dayPlanImages = dayPlanImages;
    }

    public List<DayPlanSight> getDayPlanSights() {
        return dayPlanSights;
    }

    public void setDayPlanSights(List<DayPlanSight> dayPlanSights) {
        this.dayPlanSights = dayPlanSights;
    }
}

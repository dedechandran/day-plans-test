package app.ecommerce.ui.model;

public class DayPlanImage {
    private int dayPlanImageType;
    private int dayPlanImageResource;

    public DayPlanImage(int dayPlanImageType,int dayPlanImageResource){
        this.dayPlanImageType = dayPlanImageType;
        this.dayPlanImageResource = dayPlanImageResource;
    }

    public int getDayPlanImageType() {
        return dayPlanImageType;
    }

    public void setDayPlanImageType(int dayPlanImageType) {
        this.dayPlanImageType = dayPlanImageType;
    }

    public int getDayPlanImageResource() {
        return dayPlanImageResource;
    }

    public void setDayPlanImageResource(int dayPlanImageResource) {
        this.dayPlanImageResource = dayPlanImageResource;
    }
}

package app.ecommerce.ui.model;

public class DayPlanItem {
    private int dayPlanType;
    private Object dayPlanItem;

    public DayPlanItem(int dayPlanType,Object dayPlanItem){
        this.dayPlanType = dayPlanType;
        this.dayPlanItem = dayPlanItem;
    }

    public int getDayPlanType() {
        return dayPlanType;
    }

    public void setDayPlanType(int dayPlanType) {
        this.dayPlanType = dayPlanType;
    }

    public Object getDayPlanItem() {
        return dayPlanItem;
    }

    public void setDayPlanItem(Object dayPlanItem) {
        this.dayPlanItem = dayPlanItem;
    }


}

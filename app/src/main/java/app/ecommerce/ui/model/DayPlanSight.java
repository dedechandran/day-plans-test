package app.ecommerce.ui.model;

public class DayPlanSight {
    private int dayPlanSightDistance;
    private String dayPlanSightName;
    private int dayPlanSightDuration;
    private String dayPlanSightTime;

    public DayPlanSight(int dayPlanSightDistance, String dayPlanSightName, int dayPlanSightDuration, String dayPlanSightTime) {
        this.dayPlanSightDistance = dayPlanSightDistance;
        this.dayPlanSightName = dayPlanSightName;
        this.dayPlanSightDuration = dayPlanSightDuration;
        this.dayPlanSightTime = dayPlanSightTime;
    }

    public int getDayPlanSightDistance() {
        return dayPlanSightDistance;
    }

    public void setDayPlanSightDistance(int dayPlanSightDistance) {
        this.dayPlanSightDistance = dayPlanSightDistance;
    }

    public String getDayPlanSightName() {
        return dayPlanSightName;
    }

    public void setDayPlanSightName(String dayPlanSightName) {
        this.dayPlanSightName = dayPlanSightName;
    }

    public int getDayPlanSightDuration() {
        return dayPlanSightDuration;
    }

    public void setDayPlanSightDuration(int dayPlanSightDuration) {
        this.dayPlanSightDuration = dayPlanSightDuration;
    }

    public String getDayPlanSightTime() {
        return dayPlanSightTime;
    }

    public void setDayPlanSightTime(String dayPlanSightTime) {
        this.dayPlanSightTime = dayPlanSightTime;
    }
}
